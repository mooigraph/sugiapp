/* GNU GPL version 3 - do a sugiyama layout */

#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/SugiyamaLayout.h>

using namespace ogdf;

int main()
{
	Graph G;
	GraphAttributes GA(G,
	  GraphAttributes::nodeGraphics |
	  GraphAttributes::edgeGraphics |
	  GraphAttributes::nodeLabel |
	  GraphAttributes::edgeStyle |
	  GraphAttributes::nodeStyle |
	  GraphAttributes::nodeTemplate);
	if (!GraphIO::read(GA, G, "example.gml", GraphIO::readGML)) {
		std::cerr << "Could not load example.gml" << std::endl;
		return 1;
	}

	// We assign a width and height of 10.0 to each node.
	for (node v : G.nodes)
		GA.width(v) = GA.height(v) = 10.0;

	SugiyamaLayout SL;
	SL.setRanking(new OptimalRanking);
	SL.setCrossMin(new MedianHeuristic);

	OptimalHierarchyLayout *ohl = new OptimalHierarchyLayout;
	ohl->layerDistance(30.0);
	ohl->nodeDistance(25.0);
	ohl->weightBalancing(0.8);
	SL.setLayout(ohl);

	SL.call(GA);
	GraphIO::write(GA, "output-example.gml", GraphIO::writeGML);

	GraphIO::write(GA, "output-example.svg", GraphIO::drawSVG);

	GraphIO::write(GA, "output-example.rudy", GraphIO::writeRudy);

	GraphIO::write(GA, "output-example.dot", GraphIO::writeDOT);

	GraphIO::write(GA, "output-example.graphml", GraphIO::writeGraphML);

	GraphIO::write(GA, "output-example.tlp", GraphIO::writeTLP);


	return 0;
}

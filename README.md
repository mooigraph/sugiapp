Run a sugiyama directed graph layout algorithm using the ogdf lib on a example.gml file and write the result as a gml graph file with coordinates added.

On GNU/Linux

qmake -r
make
cd bin
sugiapp

~~~~

or Windows MinGW:
~~~~
mingw32-make 
~~~~

or Windows MSVC:
~~~~
nmake 
~~~~

or by Jom:
~~~~
jom 
~~~~

### Supported compilers

- Microsoft Visual Studio 2015 (Community Edition)
- MinGW 5.3
- GCC 4.8 & GCC 5.3 (Linux)
- GCC 6.4.0 (Cygwin) 
- Clang C++ (FreeBSD)


### Supported OSes

tested on Microsoft Windows 10 and several Linux.
It can be compiled & started under Cygwin as well.

This is GNU GPL version 3 libre software
